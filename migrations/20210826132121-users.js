"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "users",
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        createdAt: {
          type: Sequelize.DATE,
          default: Date()
        },
        updatedAt: {
          type: Sequelize.DATE,
          default: Date()
        },
        email: Sequelize.STRING,
        first_name: Sequelize.STRING,
        last_name: Sequelize.STRING,
        avatar: Sequelize.STRING,
      },
      {
        engine: "InnoDB", // default: 'InnoDB'
        charset: "utf8", // default: null
      }
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("users");
  },
};
