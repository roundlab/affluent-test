'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "reports",
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        createdAt: {
          type: Sequelize.DATE,
          default: Date()
        },
        updatedAt: {
          type: Sequelize.DATE,
          default: Date()
        },
        commissions: Sequelize.FLOAT,
        sales: Sequelize.INTEGER,
        leads: Sequelize.INTEGER,
        epc: Sequelize.FLOAT,
        impressions: Sequelize.INTEGER,
        conversion_rate: {
          type: Sequelize.FLOAT
        },
        report_date: Sequelize.DATE,
      },
      {
        engine: "InnoDB", // default: 'InnoDB'
        charset: "utf8", // default: null
      }
    );
  },

  down: async (queryInterface, Sequelize) => { 
    await queryInterface.dropTable('reports');
  }
};
