import puppeteer from "puppeteer";
import { ImportFromApi } from "./services/import-api.service";
import { ImportFromWebsite } from "./services/import-website.service";
import { parsingConfig } from "./config/parsing.config";
import { apiConfig } from "./config/api.config";

const importFromApi = new ImportFromApi(apiConfig.url);
importFromApi.doIt().then((data) => console.log(data));

const importFromWebsite = new ImportFromWebsite(parsingConfig, puppeteer);
importFromWebsite.doIt().then((data) => console.log(data));
