# Afflient Test Case

### Stack

- Node.js
- ORM, migrations, seeds - Sequilizer
- Language - TypeScript
- library - currency.js


### Install dependencies

- npm i
- docker-compose up

### Check config
- database - `config/db.config.ts`
- parser - `config/parsing.config.ts`

### Prepare database

- npm run migrate

### Start

- npm run start

### Troubleshooting
- npm run test

