import { DataTypes, Model } from "sequelize";
import { sequelize } from "../config/db.config";

export class Report extends Model {}

Report.init(
  {
    id: { type: DataTypes.INTEGER, primaryKey: true },
    commissions: DataTypes.FLOAT,
    sales: DataTypes.NUMBER,
    leads: DataTypes.NUMBER,
    epc: DataTypes.FLOAT,
    impressions: DataTypes.NUMBER,
    conversion_rate: DataTypes.FLOAT,
    report_date: DataTypes.DATE,
  },
  { sequelize, modelName: "report" }
);
