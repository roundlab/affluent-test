import { DataTypes, Model } from "sequelize";
import { sequelize } from "../config/db.config";

export class User extends Model {}
User.init(
  {
    id: { type: DataTypes.INTEGER, primaryKey: true },
    first_name: DataTypes.TEXT,
    last_name: DataTypes.TEXT,
    email: DataTypes.TEXT,
    avatar: DataTypes.TEXT,
  },
  { sequelize, modelName: "user" }
);
