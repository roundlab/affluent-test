import { ImportFromApi } from "../import-api.service";
import { User } from "../../models/user";
import { apiConfig } from "../../config/api.config";
import fetch from "node-fetch";

test("check resource availability", async() => {
    const response = await(await fetch(apiConfig.url)).json();
    expect(response).toBeTruthy;
});

User.destroy({ truncate: true });

test("get from json first user email", async () => {
  const importFromApi = new ImportFromApi(apiConfig.url);
  const users = await importFromApi.doIt();
  expect(users[0].email).toMatch(/[a-z]+/);
});

test("get from database random user email", async () => {
  const oneUser = await User.findOne({ attributes: ["email"] });
  expect(oneUser.get("email")).toMatch(/[a-z]+/);
});
