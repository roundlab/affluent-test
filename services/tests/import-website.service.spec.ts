import { ImportFromWebsite } from "../import-website.service";
import { User } from "../../models/user";
import { parsingConfig } from "../../config/parsing.config";
import fetch from "node-fetch";
import puppeteer from "puppeteer";
import { Report } from "../../models/report";

test("check resource availability", async() => {
    const response = await fetch(parsingConfig.startUrl);
    expect(response).toBeTruthy;
});

Report.destroy({ truncate: true });

test("get from json first report commissions", async () => {
  const importFromWebsite = new ImportFromWebsite(parsingConfig, puppeteer);
  const reports = await importFromWebsite.doIt();
  expect(reports[0].commissions).toBeGreaterThan(0);
}, 30000);

test("get from database random report commissions", async () => {
  const oneReport = await Report.findOne({ attributes: ["commissions"] });
  expect(oneReport.get("commissions").toString()).toMatch(/[0-9]+/);
});



