import { doIt, IResponse, IUser } from "../interfaces/types.d";
import { User } from "../models/user";
import fetch from "node-fetch";

export class ImportFromApi implements doIt {
  constructor(protected apiUrl: string) {}

  public async doIt() {
    const userData = await this.getFromApi();
    await this.insertToDb(userData);
    return userData;
  }

  protected async getFromApi() {
    return (async () => {
      const firstResponse: IResponse = await (
        await fetch(`${this.apiUrl}?page=1`)
      ).json();
      let userData = firstResponse.data;

      for (let i = 2; i <= firstResponse.total_pages; i++) {
        let response: IResponse = await (
          await fetch(`${this.apiUrl}?page=${i}`)
        ).json();
        userData.push(...response.data);
      }

      return userData;
    })();
  }

  protected async insertToDb(userData: IUser[]) {
    const creatingReport = await User.bulkCreate(userData, {
      ignoreDuplicates: true,
    });
    return creatingReport;
  }
}
