import {
  Page,
  Browser,
  doIt,
  IConfig,
  ILaunch,
  IReport,
} from "../interfaces/types.d";
import { Report } from "../models/report";
import currency from "currency.js";

export class ImportFromWebsite implements doIt {
  constructor(protected config: IConfig, protected puppeteer: ILaunch) {}

  public doIt(): Promise<IReport[]> {
    return (async () => {
      let { page, browser } = await this.openSite(this.config.startUrl);
      page = await this.fillAuthForm(
        page,
        this.config.login,
        this.config.password
      );
      let data = await this.getData(page, this.config.dataUrl);
      if (this.config.closeBrowser) {
        browser.close();
      }
      const preparedData = this.prepareData(data);

      await this.insertToDb(preparedData);

      return preparedData;
    })();
  }

  protected openSite(
    website: string
  ): Promise<{ page: Page; browser: Browser }> {
    return (async () => {
      const browser = await this.puppeteer.launch({
        headless: this.config.headless,
      });
      const page = await browser.newPage();
      await page.goto(website);

      return { page, browser };
    })();
  }

  protected fillAuthForm(
    page: Page,
    login: string,
    password: string
  ): Promise<Page> {
    return (async () => {
      await page.focus("input[name=username]");
      await page.keyboard.type(login);
      await page.focus("input[name=password]");
      await page.keyboard.type(password);
      await page.keyboard.press("Enter");
      await this.sleep(3000);
      return page;
    })();
  }

  protected getData(page: Page, dataUrl: string): Promise<any> {
    return (async () => {
      await page.goto(dataUrl);
      await this.sleep(3000);
      await page.click(".dataTables_length");
      await this.sleep(1000);
      const [button] = await page.$x(
        "//div[contains(@class,'dataTables_length')]//span[contains(., 'All')]"
      );
      if (button) {
        await button.click();
      }
      await this.sleep(3000);
      const data = await page.evaluate(() =>
        Array.from(
          document.querySelectorAll("table[data-url=dates] > tbody > tr"),
          (row) =>
            Array.from(
              row.querySelectorAll("th, td"),
              (cell) => cell.textContent
            )
        )
      );

      return data;
    })();
  }

  protected prepareData(data: string[][]): IReport[] {
    const columns = this.config.tableColumns;
    const preparedData: IReport[] = data.map((row) => ({
      commissions: currency(row[columns.COMMISSIONS]).value,
      sales: this.getNumber(row[columns.SALES]),
      leads: this.getNumber(row[columns.LEADS]),
      epc: currency(row[columns.EPC]).value,
      clicks: this.getNumber(row[columns.CLICKS]),
      impressions: this.getNumber(row[columns.IMPRESSIONS]),
      conversion_rate: parseFloat(row[columns.CR]) / 100,
      report_date:
        Date.parse(row[columns.DATE]) +
        this.config.timeZoneOffsetInHours * 3600000,
    }));

    return preparedData;
  }

  protected async insertToDb(userData: IReport[]) {
    const creatingReport = await Report.bulkCreate(userData, {
      ignoreDuplicates: true,
    });
    return creatingReport;
  }

  protected getNumber(str: string) {
    return parseInt(str.replace(/,/g, ""));
  }

  protected sleep(ms: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }
}
