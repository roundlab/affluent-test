import {
  LaunchOptions,
  BrowserLaunchArgumentOptions,
  BrowserConnectOptions,
  Product,
  Browser,
} from "puppeteer/lib/types.d";
export { Page, Browser } from "puppeteer/lib/types.d";

export interface IResponse {
  page: number;
  per_page: number;
  total: number;
  total_pages: number;
  data: IUser[];
}

export interface IUser {
  id?: number;
  first_name: string;
  last_name: string;
  email: string;
  avatar: string;
}

export interface IReport {
  id?: number,
  commissions: number,
  sales: number,
  leads: number,
  clicks: number,
  epc: number,
  impressions: number,
  conversion_rate: number,
  report_date?: Date | number
}

export interface doIt {
  doIt(): Promise<any>;
}

type Columns =
  | "DATE"
  | "COMMISSIONS"
  | "SALES"
  | "LEADS"
  | "CLICKS"
  | "EPC"
  | "IMPRESSIONS"
  | "CR";

export interface IConfig {
  closeBrowser: boolean;
  headless: boolean;
  startUrl: string;
  login: string;
  password: string;
  dataUrl: string;
  timeZoneOffsetInHours: number;
  tableColumns: { [key in Columns]: number };
}

export interface ILaunch {
  launch: (
    options?: LaunchOptions &
      BrowserLaunchArgumentOptions &
      BrowserConnectOptions & {
        product?: Product;
        extraPrefsFirefox?: Record<string, unknown>;
      }
  ) => Promise<Browser>;
}
