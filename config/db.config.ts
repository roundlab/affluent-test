import { Sequelize } from "sequelize";

export const DbConfig = {
  development: {
    username: "user",
    password: "password",
    database: "db",
    host: "localhost",
    port: 8001,
    dialect: "mysql",
  },
};
const currentDbConfig = DbConfig.development;

export const sequelize = new Sequelize(
  currentDbConfig.database,
  currentDbConfig.username,
  currentDbConfig.password,
  {
    port: currentDbConfig.port,
    dialect: "mysql",
    host: currentDbConfig.host,
  }
);
