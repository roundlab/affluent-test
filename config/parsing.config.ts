import { IConfig } from "../interfaces/types";

export const parsingConfig: IConfig = {
  headless: true,
  closeBrowser: true,
  startUrl: "https://develop.pub.afflu.net",
  login: "developertest@affluent.io",
  password: "Wn4F6g*N88EPiOyW",
  timeZoneOffsetInHours: 3,
  dataUrl:
    "https://develop.pub.afflu.net/list?type=dates&startDate=2020-10-01&endDate=2020-10-30",
  tableColumns: {
    DATE: 0,
    COMMISSIONS: 1,
    SALES: 2,
    LEADS: 3,
    CLICKS: 4,
    EPC: 5,
    IMPRESSIONS: 6,
    CR: 7,
  },
};
